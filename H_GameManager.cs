﻿using HarmonyLib;
using Platform;
using UnityEngine;

namespace UpdateTimeWithoutPlayers
{
    public class H_GameManager
    {
        [HarmonyPatch(typeof(GameManager), nameof(GameManager.updateTimeOfDay))]
        public class H_GameManager_updateTimeOfDay
        {
            [HarmonyPrefix]
            static bool updateTimeOfDay(GameManager __instance)
            {
                float v1 = 1000f / (float)GameStats.GetInt(EnumGameStats.TimeOfDayIncPerSec);
                __instance.msPassedSinceLastUpdate += (int)((double)Time.deltaTime * 1000.0);
                if ((double)__instance.msPassedSinceLastUpdate <= (double)Utils.FastMax(v1, 50f))
                    return false;
                int num = (int)((double)__instance.msPassedSinceLastUpdate / (double)v1);
                __instance.msPassedSinceLastUpdate -= (int)v1 * num;
                __instance.m_World.SetTime(__instance.m_World.worldTime + (ulong)num);

                PlatformManager.NativePlatform.LobbyHost?.UpdateGameTimePlayers(__instance.m_World.worldTime, __instance.m_World.Players.list.Count);
                GameSenseManager.Instance?.UpdateEventTime(__instance.m_World.worldTime);
                if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer || (double)Time.time - (double)__instance.lastTimeWorldTickTimeSentToClients <= (double)Constants.cSendWorldTickTimeToClients)
                    return false;
                SingletonMonoBehaviour<ConnectionManager>.Instance.LocalServerInfo.UpdateGameTimePlayers(__instance.m_World.worldTime, __instance.m_World.Players.list.Count);
                __instance.lastTimeWorldTickTimeSentToClients = Time.time;
                SingletonMonoBehaviour<ConnectionManager>.Instance.SendPackage((NetPackage)NetPackageManager.GetPackage<NetPackageWorldTime>().Setup(__instance.m_World.worldTime), true);
                if (!((UnityEngine.Object)WeatherManager.Instance != (UnityEngine.Object)null))
                    return false;
                WeatherManager.Instance.SendPackages();
                return false;
            }
        }
    }
}