﻿using HarmonyLib;
using System.Reflection;

namespace UpdateTimeWithoutPlayers
{
    public class ModApi : IModApi
    {
        public void InitMod(Mod _mod) => Harmony.CreateAndPatchAll(Assembly.GetExecutingAssembly());
    }
}
